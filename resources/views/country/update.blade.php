@extends('theme.default')

@section('title', 'Update Country')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Update Country</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{ route('country.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                     {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PATCH">

                   <div class="form-group">
                     <label>Country Name</label>
                     <input type="text" name="c_name" class="form-control" value="@isset($data->c_name){{$data->c_name}}@endisset" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="c_description" class="form-control" value="@isset($data->c_description){{$data->c_description}}@endisset" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>Country Code</label>
                     <input type="number" name="c_code" class="form-control" value="@isset($data->c_code){{$data->c_code}}@endisset" placeholder="Enter Mobile" >
                  </div>
                  <div class="form-group">
                     <img src="{{asset('images/'.$data->c_flag)}}" alt="" width="30px"><br>
                     <label>Flaf</label>
                     <input type="file" name="c_flag">
                  </div>
                  <div class="reset-button">
                     <input type="submit" name="save" id="save" class="btn btn-success" value="Save">
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection