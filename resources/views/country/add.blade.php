@extends('theme.default')

@section('title', 'Add Employee')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add Country</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{route('country.store')}}" method="POST" enctype="multipart/form-data">

                   @csrf

                   <div class="form-group">
                     <label>Country Name</label>
                     <input type="text" name="c_name" class="form-control" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="c_description" class="form-control" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>Country Code</label>
                     <input type="number" name="c_code" class="form-control" placeholder="Enter Email" >
                  </div>
                  <div class="form-group">
                     <label>Flag</label>
                     <input type="file" name="c_flag">
                  </div>
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection