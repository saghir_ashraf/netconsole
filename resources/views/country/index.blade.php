@extends('theme.default')

@section('title', 'Add Country')


@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<a href="{{route('country.create')}}" class="btn btn-success">Add Country</a>	
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tbody >
						<tr>
							<th>Sr No.</th>
							<th>Country Name</th>
							<th>Description</th>
							<th>Country Code</th>
							<th>Flag</th>
							<th>Status</th>
							<th>Action</th>
						</tr>

						@if(isset ($country))
						
						@foreach ($country as $value)

						<tr>
							<td>{{$value->id}}</td>
							<td>{{$value->c_name}}</td>
							<td>{{$value->c_description}}</td>
							<td>{{$value->c_code}}</td>
							<td><img src="{{asset('images/'.$value->c_flag)}}" alt="" width="30px"></td>
							<td>
								<form action="{{route('country.status')}}" method="POST">
									@csrf
									<input type="hidden" name="id" value="{{$value->id}}">
									<input type="hidden" name="status" value="{{$value->status}}">
									@if($value->status==1)
									<input type="submit" class="btn btn-success" value="Active">
									@else
									<input type="submit" class="btn btn-danger" value="Deactive">
									@endif
								</form>
							</td>
							<td>
								<a href="{{route('country.edit',$value->id)}}" class="btn btn-success">Edit</a>
								<a href="{{route('country.delete_data',$value->id)}}" class="btn btn-danger">Delete</a>
							</td>
						</tr> 
						@endforeach
						@endif   
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection