<!DOCTYPE html>
<html>
<head>
  <title>magExpress</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/slick.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('user/assets/css/style.css')}}">
<!--[if lt IE 9]>
<script src="{{asset('user/assets/js/html5shiv.min.js')}}"></script>
<script src="{{asset('user/assets/js/respond.min.js')}}"></script>
<![endif]-->
</head>
<body>
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <div class="container">
    @section('header')
    @include('users.theme.header')
    @show
    
    @section('navbar')
    @include('users.theme.navbar')
    @show

    @yield('content')


    <footer id="footer">
     @section('footer')
     @include('users.theme.footer')
     @show
   </footer>
   <script src="{{asset('user/assets/js/jquery.min.js')}}"></script> 
   <script src="{{asset('user/assets/js/bootstrap.min.js')}}"></script> 
   <script src="{{asset('user/assets/js/wow.min.js')}}"></script> 
   <script src="{{asset('user/assets/js/slick.min.js')}}"></script> 
   <script src="{{asset('user/assets/js/custom.js')}}"></script>
 </body>
 </html>