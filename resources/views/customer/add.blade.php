@extends('theme.default')

@section('title', 'Add Customer')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add Customer</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{route('customer.store')}}" method="POST" enctype="multipart/form-data">

                   @csrf

                   <div class="form-group">
                     <label>First Name</label>
                     <input type="text" name="fname" class="form-control" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Last Name</label>
                     <input type="text" name="lname" class="form-control" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>Email</label>
                     <input type="email" name="email" class="form-control" placeholder="Enter Email" >
                  </div>
                  <div class="form-group">
                     <label>Mobile</label>
                     <input type="number" name="mobile" class="form-control" placeholder="Enter Mobile" >
                  </div>
                  <div class="form-group">
                     <label>Picture upload</label>
                     <input type="file" name="picture" >
                  </div>
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  <div class="notification is-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                     </ul>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection