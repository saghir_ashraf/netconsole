
@extends('theme.default')

@section('title', 'Search')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('customer.create')}}" class="btn btn-success">Add Customer</a>
      </div>

      <form action="{{route('customer.search')}}" method="POST">
       @csrf
       <label for="search">Search:</label>
       <input type="text" id="search" name="search">
       <input type="submit" value="Search" class="btn-success">
     </form>
     <!--  <div class="box-header with-border">
       <label for="search">Search:</label>
      <input type="text" id="search" name="search">
      <a href="{{route('customer.search')}}" class="btn btn-success">Search</a>
    </div> -->
    <!-- /.box-header -->


    <div class="box-body">
      <table class="table table-bordered">
        <tbody>
          <tr>
           <th>Sr.No</th>
           <th>First Name</th>
           <th>Last Name</th>
           <th>Mobile No.</th>
           <th>Email</th>
           <th>Image</th>
           <th>Status</th>
           <th>Action</th>
         </tr>

         @if(isset ($customer))

         @foreach ($customer as $value)

         <tr>
           <td>{{$value->id}}</td>
           <td>{{$value->fname}}</td>
           <td>{{$value->lname}}</td>
           <td>{{$value->mobile}}</td>
           <td>{{$value->email}}</td>
           <td><img src="{{asset('images/'.$value->picture)}}" alt="" width="30px"></td>
           <td>
            <form action="{{route('customer.status')}}" method="POST">
             @csrf
             <input type="hidden" name="id" value="{{$value->id}}">
             <input type="hidden" name="status" value="{{$value->status}}">
             @if($value->status == 1)
             <input type="submit" class="btn btn-success" value="Active">
             @else
             <input type="submit" class="btn btn-danger" value="Deactive">
             @endif
           </form>
         </td>
         <td><a href="{{route('customer.edit',$value->id)}}" class="btn btn-success">Edit</a>
          <a href="{{route('customer.delete_data',$value->id)}}" class="btn btn-danger">Delete</a> </td> 
        </tr>

        @endforeach
        @endif
      </tbody>
    </table>
  </div>

  <!-- /.box-body -->

</div>
<!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection