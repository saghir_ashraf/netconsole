@extends('theme.default')

@section('title', 'Home Slider')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('home_slider.create')}}" class="btn btn-success">Add Home Slider</a>
      </div>
      <form action="{{route('home_slider.search')}}" method="POST">
       @csrf
       <label for="search">Search:</label>
       <input type="text" id="search" name="search">
       <input type="submit" value="Search" class="btn-success">
     </form>
      <!-- /.box-header -->


      <div class="box-body">
        <table class="table table-bordered">
          <tbody>
            <tr>
             <th>Sr.No</th>
             <th>Image</th>
             <th>Paragraph</th>
             <th>Heading</th>
             <th>Description</th>
             <th>Status</th>
             {{-- <th>Status</th> --}}
             <th>Action</th>
           </tr>

          

           @foreach ($home_slider as $value)

           <tr>
             <td>{{$value->id}}</td>
             <td><img src="{{asset('images/'.$value->image)}}" alt="" width="30px"></td>
             <td>{{$value->paragraph}}</td>
             <td>{{$value->heading}}</td>
             <td>{{$value->description}}</td>
             <td>
              <form action="{{route('home_slider.status')}}" method="POST">
               @csrf
               <input type="hidden" name="id" value="{{$value->id}}">
               <input type="hidden" name="status" value="{{$value->status}}">
               @if($value->status == 1)
               <input type="submit" class="btn btn-success" value="Active">
               @else
               <input type="submit" class="btn btn-danger" value="Deactive">
               @endif
             </form>
           </td>
           {{-- <td>{{$value->status}}</td> --}}
           <td><a href="{{route('home_slider.edit',$value->id)}}" class="btn btn-success">Edit</a>
            <a href="{{route('home_slider.delete_data',$value->id)}}" class="btn btn-danger">Delete</a> </td> 
          </tr>
          @endforeach
        
        </tbody>
      </table>
    </div>

    <!-- /.box-body -->

  </div>
  <!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection