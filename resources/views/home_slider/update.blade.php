@extends('theme.default')

@section('title', 'Add Home Slider')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add Home Slider</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                <form class="col-sm-8" action="{{route('home_slider.update',$data->id)}}" method="POST" enctype="multipart/form-data">
                   @csrf
                <input name="_method" type="hidden" value="PATCH">
  
                   <div class="form-group">
                     <div class="form-group">
                      <img src="{{asset('images/'.$data->image)}}" alt="" width="30px"><br>
                     <label>Picture upload</label>
                     <input type="file" name="image" >
                  </div>
                     <label>Paragraph</label>
                     <input type="text" name="paragraph" class="form-control" value="@isset($data->paragraph){{$data->paragraph}}@endisset" placeholder="Paragraph" >
                  </div>
                  <div class="form-group">
                     <label>Heading</label>
                     <input type="text" name="heading" class="form-control" value="@isset($data->heading){{$data->heading}}@endisset" placeholder="Heading" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="description" class="form-control" value="@isset($data->description){{$data->description}}@endisset" placeholder="Enter Email" >
                  </div>
                  <!-- <div class="form-group">
                     <label>Mobile</label>
                     <input type="number" name="mobile" class="form-control" placeholder="Enter Mobile" >
                  </div> -->
                  
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  <!-- <div class="notification is-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                     </ul>
                  </div> -->
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection