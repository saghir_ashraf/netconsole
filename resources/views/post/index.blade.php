@extends('theme.default')

@section('title', 'Add Post')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('post.create')}}" class="btn btn-success">Add post</a>
      </div>

      <form action="{{route('post.search')}}" method="POST">
       @csrf
       <label for="search">Search:</label>
       <input type="text" id="search" name="search">
       <input type="submit" value="Search" class="btn-success">
     </form>

    <div class="box-body">
      <table class="table table-bordered">
        <tbody>
          <tr>
           <th>Sr.No</th>
           <th>Category Name</th>
           <th>Name</th>
           <th>Description</th>
           <th>Image</th>
           <th>Status</th>
           <th>Action</th>
         </tr>

         @if(isset ($post))

         @foreach ($post as $value)

         <tr>
           <td>{{$value->id}}</td>
           <td>{{$value->category->catName}}</td>
           <td>{{$value->name}}</td>
           <td>{{$value->description}}</td>
           <td><img src="{{asset('images/'.$value->image)}}" alt="" width="30px"></td>
          
           <td>
            <form action="{{route('post.status')}}" method="POST">
             @csrf
             <input type="hidden" name="id" value="{{$value->id}}">
             <input type="hidden" name="status" value="{{$value->status}}">
             @if($value->status == 1)
             <input type="submit" class="btn btn-success" value="Active">
             @else
             <input type="submit" class="btn btn-danger" value="Deactive">
             @endif
           </form>
         </td>
         <td><a href="{{route('post.edit',$value->id)}}" class="btn btn-success">Edit</a>
          <a href="{{route('post.delete_data',$value->id)}}" class="btn btn-danger">Delete</a> </td> 
        </tr>

        @endforeach
        @endif
      </tbody>
    </table>
  </div>

  <!-- /.box-body -->

</div>
<!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection