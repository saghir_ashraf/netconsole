@extends('theme.default')

@section('title', 'Add post')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add Post</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">

                    @csrf
                     
                    <select name="catId" class="form-control">
                     @if(isset($category) && count($category)>0 )
                     @foreach($category as $value)
                     
                     <option value="{{$value->id}}">{{$value->catName}}</option>
                     @endforeach
                     @endif
                  </select>

                  <div class="form-group">
                     <label>Name</label>
                     <input type="text" name="name" class="form-control" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="description" class="form-control" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>Picture upload</label>
                     <input type="file" name="image" >
                  </div>
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  <div class="notification is-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                     </ul>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection