@extends('theme.default')

@section('title', 'Add Category')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('category.create')}}" class="btn btn-success">Add Category</a>
     </div>
     <!-- /.box-header -->
     
       
     <div class="box-body">
        <table class="table table-bordered">
          <tbody>
            <tr>
               <th>Sr.No</th>
               <th>Category Name</th>
               <th>Description</th>
               <th>Status</th>
               <th>Action</th>
            </tr>

            @if(isset ($category))

            @foreach ($category as $value)

            <tr>
               <td>{{$value->id}}</td>
               <td>{{$value->catName}}</td>
               <td>{{$value->description}}</td>
               <td>
                <form action="{{route('category.status')}}" method="POST">
                   @csrf
               <input type="hidden" name="id" value="{{$value->id}}">
               <input type="hidden" name="status" value="{{$value->status}}">
               @if($value->status == 1)
               <input type="submit" class="btn btn-success" value="Active">
                @else
               <input type="submit" class="btn btn-danger" value="Deactive">
                @endif
                </form>
              </td>
               <td>
                <a href="{{route('category.edit',$value->id)}}" class="btn btn-success">Edit</a>
                {{-- <a href="{{route('admin.delete_data',$value->id)}}" class="btn btn-danger">Delete</a>  --}}
               </td> 
               </tr>

               @endforeach
               @endif
            </tbody>
         </table>
      </div>
        
      <!-- /.box-body -->

   </div>
   <!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection