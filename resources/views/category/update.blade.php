@extends('theme.default')

@section('title', 'Add Admin')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add Category</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{route('category.update', $data->id)}}" method="POST" enctype="multipart/form-data">

                   @csrf
                   <input name="_method" type="hidden" value="PATCH">

                   <div class="form-group">
                     <label>Category Name</label>
                     <input type="text" name="catName" class="form-control" value="@isset($data->catName){{$data->catName}}@endisset" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="description" class="form-control" value="@isset($data->description){{$data->description}}@endisset" placeholder="Enter last Name" >
                  </div>
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  <!-- <div class="notification is-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                     </ul>
                  </div> -->
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection