@extends('theme.default')

@section('title', 'Add Admin')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('admin.create')}}" class="btn btn-success">Add Admin</a>
     </div>
     <!-- /.box-header -->
     
       
     <div class="box-body">
        <table class="table table-bordered">
          <tbody>
            <tr>
               <th>Sr.No</th>
               <th>First Name</th>
               <th>Last Name</th>
               <th>Mobile No.</th>
               <th>Email</th>
               <th>Image</th>
               <th>Status</th>
               <th>Action</th>
            </tr>

            @if(isset ($admin))

            @foreach ($admin as $value)

            <tr>
               <td>{{$value->id}}</td>
               <td>{{$value->fname}}</td>
               <td>{{$value->lname}}</td>
               <td>{{$value->mobile}}</td>
               <td>{{$value->email}}</td>
               <td><img src="{{asset('images/'.$value->picture)}}" alt="" width="30px"></td>
               <td>
                <form action="{{route('admin.status')}}" method="POST">
                   @csrf
               <input type="hidden" name="id" value="{{$value->id}}">
               <input type="hidden" name="status" value="{{$value->status}}">
               @if($value->status == 1)
               <input type="submit" class="btn btn-success" value="Active">
                @else
               <input type="submit" class="btn btn-danger" value="Deactive">
                @endif
                </form>
              </td>
               <td><a href="{{route('admin.edit',$value->id)}}" class="btn btn-success">Edit</a>
                  <a href="{{route('admin.delete_data',$value->id)}}" class="btn btn-danger">Delete</a> </td> 
               </tr>

               @endforeach
               @endif
            </tbody>
         </table>
      </div>
        
      <!-- /.box-body -->

   </div>
   <!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection