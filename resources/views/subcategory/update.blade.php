@extends('theme.default')

@section('title', 'Add post')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add post</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{ route('sub_cat.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                     {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PATCH">
                  
                  <select name="catId" class="form-control">
                     @if(isset($category) && count($category)>0 )
                     @foreach($category as $value)
                     
                        <option value="{{$value->id}}" @if($data->catId==$value->id) selected @endif >{{$value->catName}}</option>
                     @endforeach
                     @endif
                  </select>

                   <div class="form-group">
                     <label>Name</label>
                     <input type="text" name="name" class="form-control" value="@isset($data->name){{$data->name}}@endisset" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="description" class="form-control" value="@isset($data->description){{$data->description}}@endisset" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <img src="{{asset('images/'.$data->image)}}" alt="" width="30px"><br>
                     <label>Picture upload</label>
                     <input type="file" name="image">
                  </div>
                  <div class="reset-button">
                     <input type="submit" name="save" id="save" class="btn btn-success" value="Save">
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection