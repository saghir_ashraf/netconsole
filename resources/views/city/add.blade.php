@extends('theme.default')

@section('title', 'Add City')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add City</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{route('city.store')}}" method="POST" enctype="multipart/form-data">

                    @csrf
                     
                    <select name="countryId" class="form-control">
                     @if(isset($country) && count($country)>0 )
                     @foreach($country as $value)
                  
                     <option value="{{$value->id}}">{{$value->c_name}}</option>
                     @endforeach
                     @endif
                  </select>

                  <div class="form-group">
                     <label>City Name</label>
                     <input type="text" name="cityName" class="form-control" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>City Description</label>
                     <input type="text" name="cityDescription" class="form-control" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>City Code</label>
                     <input type="number" name="cityCode" class="form-control" placeholder="Enter Email" >
                  </div>
                  {{-- <div class="form-group">
                     <label>Picture upload</label>
                     <input type="file" name="image" >
                  </div> --}}
                  <div class="reset-button">
                     <input type="submit"  class="btn btn-success" value="Save">
                  </div>
                  <div class="notification is-danger">
                     <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                     </ul>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection