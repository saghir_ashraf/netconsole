@extends('theme.default')

@section('title', 'Add city')


@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">

        <a href="{{route('city.create')}}" class="btn btn-success">Add City</a>
      </div>

      <form action="{{route('city.search')}}" method="POST">
       @csrf
       <label for="search">Search:</label>
       <input type="text" id="search" name="search">
       <input type="submit" value="Search" class="btn-success">
     </form>

    <div class="box-body">
      <table class="table table-bordered">
        <tbody>
          <tr>
           <th>Sr.No</th>
           <th>Country Name</th>
           <th>City Name</th>
           <th>City Description</th>
           <th>City Code</th>
           <th>Status</th>
           <th>Action</th>
         </tr>

         @if(isset ($city))

         @foreach ($city as $value)

         <tr>
           <td>{{$value->id}}</td>
           <td>{{$value->country->c_name}}</td>
           <td>{{$value->cityName}}</td>
           <td>{{$value->cityDescription}}</td>
           <td>{{$value->cityCode}}</td>
           
           <td>
            <form action="{{route('city.status')}}" method="POST">
             @csrf
             <input type="hidden" name="id" value="{{$value->id}}">
             <input type="hidden" name="status" value="{{$value->status}}">
             @if($value->status == 1)
             <input type="submit" class="btn btn-success" value="Active">
             @else
             <input type="submit" class="btn btn-danger" value="Deactive">
             @endif
           </form>
         </td>
         <td><a href="{{route('city.edit',$value->id)}}" class="btn btn-success">Edit</a>
          <a href="{{route('city.delete_data',$value->id)}}" class="btn btn-danger">Delete</a> </td> 
        </tr>

        @endforeach
        @endif
      </tbody>
    </table>
  </div>

  <!-- /.box-body -->

</div>
<!-- /.box -->


</div>
<!-- /.col -->
</div>


@endsection