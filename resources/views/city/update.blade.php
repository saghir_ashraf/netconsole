@extends('theme.default')

@section('title', 'Add city')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Add city</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{ route('city.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                     {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PATCH">
                  
                  <select name="countryId" class="form-control">
                     @if(isset($country) && count($country)>0 )
                     @foreach($country as $value)
                     
                        <option value="{{$value->id}}" @if($data->countryId==$value->id) selected @endif >{{$value->c_name}}</option>
                     @endforeach
                     @endif
                  </select>

                   <div class="form-group">
                     <label>Name</label>
                     <input type="text" name="cityName" class="form-control" value="@isset($data->cityName){{$data->cityName}}@endisset" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Description</label>
                     <input type="text" name="cityDescription" class="form-control" value="@isset($data->cityDescription){{$data->cityDescription}}@endisset" placeholder="Enter last Name" >
                  </div>

                  <div class="form-group">
                     <label>City Code</label>
                     <input type="number" name="cityCode" class="form-control" value="@isset ($data->cityCode){{$data->cityCode}}@endisset" placeholder="Enter Email" >
                  </div>

                  <div class="reset-button">
                     <input type="submit" name="save" id="save" class="btn btn-success" value="Save">
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection