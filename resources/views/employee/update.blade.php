@extends('theme.default')

@section('title', 'Add Employee')


@section('content')


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-title">
         <h1>Update Employee</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">

      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">

                  <form class="col-sm-8" action="{{ route('employee.update',$data->id) }}" method="POST" enctype="multipart/form-data">
                     {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PATCH">

                   <div class="form-group">
                     <label>First Name</label>
                     <input type="text" name="fname" class="form-control" value="@isset($data->fname){{$data->fname}}@endisset" placeholder="Enter First Name" >
                  </div>
                  <div class="form-group">
                     <label>Last Name</label>
                     <input type="text" name="lname" class="form-control" value="@isset($data->lname){{$data->lname}}@endisset" placeholder="Enter last Name" >
                  </div>
                  <div class="form-group">
                     <label>Email</label>
                     <input type="email" name="email" class="form-control" value="@isset($data->email){{$data->email}}@endisset" placeholder="Enter Email" >
                  </div>
                  <div class="form-group">
                     <label>Mobile</label>
                     <input type="number" name="mobile" class="form-control" value="@isset($data->mobile){{$data->mobile}}@endisset" placeholder="Enter Mobile" >
                  </div>
                  <div class="form-group">
                     <img src="{{asset('images/'.$data->picture)}}" alt="" width="30px"><br>
                     <label>Picture upload</label>
                     <input type="file" name="picture">
                  </div>
                  <div class="reset-button">
                     <input type="submit" name="save" id="save" class="btn btn-success" value="Save">
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
</div>



@endsection