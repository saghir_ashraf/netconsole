<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::prefix('admin')->group(function(){

Route::resource('customer', 'CustomerController');
Route::get('customer/delete/{id}', 'CustomerController@delete_data')->name('customer.delete_data');
Route::post('customer/status', 'CustomerController@status')->name('customer.status');
Route::post('customer/search', 'CustomerController@search' )->name('customer.search');


Route::resource('employee', 'EmployeeController');
Route::get('employee/delete/{id}', 'EmployeeController@delete_data')->name('employee.delete_data');
Route::post('employee/status', 'EmployeeController@status')->name('employee.status');

Route::resource('admin', 'AdminController');
Route::get('admin/delete/{id}', 'CustomerController@delete_data')->name('admin.delete_data');
Route::post('admin/status', 'CustomerController@status')->name('admin.status');



Route::resource('/home_slider', 'HomeSliderController');
Route::get('home_slider/delete/{id}', 'HomeSliderController@delete_data')->name('home_slider.delete_data');
Route::post('home_slider/status', 'HomeSliderController@status')->name('home_slider.status');
Route::post('home_slider/search', 'HomeSliderController@search' )->name('home_slider.search');

Route::resource('post', 'PostController');
Route::get('post/delete/{id}', 'PostController@delete_data')->name('post.delete_data');
Route::post('post/status', 'PostController@status')->name('post.status');
Route::post('post/search', 'PostController@search' )->name('post.search');

Route::resource('sub_cat', 'SubCategoryController');
Route::get('sub_cat/delete/{id}', 'SubCategoryController@delete_data')->name('sub_cat.delete_data');
Route::post('sub_cat/status', 'SubCategoryController@status')->name('sub_cat.status');
Route::post('sub_cat/search', 'SubCategoryController@search' )->name('sub_cat.search');


Route::resource('country', 'CountryController');
Route::get('country/delete/{id}', 'CountryController@delete_data')->name('country.delete_data');
Route::post('country/status', 'CountryController@status')->name('country.status');
// Route::post('country/search', 'CountryController@search' )->name('country.search');


Route::resource('city', 'CityController');
Route::get('city/delete/{id}', 'CityController@delete_data')->name('city.delete_data');
Route::post('city/status', 'CityController@status')->name('city.status');
Route::post('city/search', 'CityController@search' )->name('city.search');

});



Route::resource('/home','HomeController');
Route::get('home/slider-detail/{slug}','HomeController@slider_detail')->name('home_slider_detail');
Route::get('home/post-detail/{slug}','HomeController@post_detail')->name('home_post_detail');


Route::resource('/contact', 'ContactController');


Route::resource('/single', 'SingleController');


Route::resource('/error', 'ErrorController');


Route::resource('/about_us', 'About_usController');




Route::resource('category', 'CategoryController');
Route::post('category/status', 'CategoryController@status')->name('category.status');












