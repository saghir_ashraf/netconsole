<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'Post';

    public function category(){
    	return $this->belongsTo(Category::class,"catId");
    }
}
