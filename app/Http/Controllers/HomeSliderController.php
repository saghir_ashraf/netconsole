<?php

namespace App\Http\Controllers;

use App\HomeSlider;
use Illuminate\Http\Request;

class HomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home_slider = HomeSlider::get();
        // dd($home_slider->all());
        return view('home_slider.index', compact('home_slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('home_slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*request()->validate([

            'image' => 'required',
            'paragraph' => 'required',
            'heading' => 'required', 
            'description' => 'required',  
        ]);*/
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(
            "image" => $image_add,
            "paragraph" => $request->paragraph,
            "heading" => $request->heading,
            "slug" => str_slug(request('heading'),'-'),
            "description" => $request->description,
        );

        $save = HomeSlider::insert($data);
        if($save)
        {
            return redirect(route('home_slider.index'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function show(HomeSlider $homeSlider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = HomeSlider::find($id);
        // dd($data);
        return view('home_slider.update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = HomeSlider::find($id);
        if($check)
        {
            if($request->hasFile('image'))
            {
                $image = $request->file('image');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['image']=$name;
            }
            // $data["image"] = request('image');
            $data["paragraph"] = request('paragraph');
            $data["heading"] = request('heading');
            $data['slug'] = str_slug(request('heading'),'-');
            $data["description"] = request('description');
    
            $updated = HomeSlider::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('home_slider.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HomeSlider  $homeSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function delete_data($id)
    {
        $check = HomeSlider::find($id); 
        if($check)
        {
             HomeSlider::whereId($id)->delete();
             return redirect(route('home_slider.index'));   
        }/*else
        {
            dd("not delete");   
        }*/

    }

    public function status(Request $request)

    {
        // dd($request->all());
        $id = request('id');
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = HomeSlider::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('home_slider.index'));       
            }   

    }

    public function search(Request $request)
    {
        $search =$request->search;
        $data = HomeSlider::where('paragraph', 'LIKE', "%$search%")
        ->orWhere('heading', 'LIKE', "%$search%")
        ->orWhere('description', 'LIKE', "%$search%")
        // ->orWhere('email', 'LIKE', "%$search%")
        ->get();


           
        
        return view('home_slider.search', compact('data'));
    }
    
}
