<?php

namespace App\Http\Controllers;

use App\Home;
use App\HomeSlider;
use App\Category;
use App\Post;
use Illuminate\Http\Request;

// use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homeSlider = HomeSlider::orderBy('id','DESC')->limit(5)->get();
        $category_1 = Category::where('slug','category-1')->with('posts')->get();
        $category_2 = Category::where('slug','category-2')->with('posts')->get();
        // dd($category_2);
        $post = Post::orderBy('id','DESC')->with('category')->limit(4)->get();

        // dd($post);
        // dd($categoryPost);
        // foreach ($categoryPost as $keys ) {
            
        // }
        // dd($keys);
        // eit;
        // $homeSlider = DB::table('home_slider')->get();
        // dd($homeSlider);
        return view('users.home.index', compact('homeSlider','category_1','post', 'category_2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
       $data =  HomeSlider::find($slug);
       dd($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home $home)
    {
        //
    }
    public function slider_detail($slug)
    {
        $data =  HomeSlider::where('slug',$slug)->first();
       dd($data);
    }
     public function post_detail($slug)
    {
        $post =  Post::where('slug',$slug)->first();
       dd($post);
    }
}

