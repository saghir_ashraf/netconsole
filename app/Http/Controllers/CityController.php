<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $city = City::with('country')->get();
        // dd($city);  
        return view('city.index', compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country= Country::where('status',1)->get();
        return view('city.add', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = array(
            "countryId" => request('countryId'),
            "cityName" => request('cityName'),
            "cityDescription" => request('cityDescription'),
            "slug" => str_slug(request('cityName'),'-'),
            "cityCode"=>$request->cityCode,
        );


        $save = City::insert($data);
        if($save)
        {
            return redirect(route('city.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = City::with('country')->find($id);
        // dd($data);
        $country= Country::get();
        // dd($country);
        return view('city.update',compact('data','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = City::find($id);   

        if($check)
        {
            $data["countryId"] = request('countryId');
            $data["cityName"] = request('cityName');
            $data["cityDescription"] = request('cityDescription');
            $data['slug'] = str_slug(request('cityName'),'-');
            $data["cityCode"] = request('cityCode');
    
            $updated = City::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('city.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }

    public function delete_data($id)
    {
        $check = City::find($id); 
        if($check)
        {
             City::whereId($id)->delete();
             return redirect(route('city.index'));   
        }else
        {
            dd("not delete");   
        }

    }
     public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = City::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('city.index'));       
            }   

    }

    
}
