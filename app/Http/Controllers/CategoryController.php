<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::with('posts')->get();
        // dd($category);
        return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            "catName" => request('catName'),
            "slug" => str_slug(request('catName'),'-'),
            "description" => $request->description
            
        );


        $save = Category::insert($data);
        if($save)
        {
            return redirect(route('category.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::find($id);
        // dd($data);
        return view('category.update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Category::find($id);   

        if($check)
        {
            $data["catName"] = request('catName');
            $data["slug"] = str_slug(request('catName'),'-');
            $data["description"] = request('description');
            
            $updated = Category::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('category.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');
        
        if (request('status')==1) {
           $data["status"] = 0;
       }else{
        $data["status"] = 1;
    }

    $updated = Category::whereId($id)->update($data);
    if($updated)
    {
        return redirect(route('category.index'));       
    }   

}
}
