<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::get();
        return view('admin.index', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required', 
            'mobile' => 'required', 
            'picture' => 'required' 
        ]);
        // dd($request->all());
        if($request->hasFile('picture'))
        {
            $image = $request->file('picture');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(
            "fname" => request('fname'),
            "lname" => $request->lname,
            "email" => $request->email,
            "mobile" => $request->mobile,
            "picture"=> $image_add,
        );


        $save = Admin::insert($data);
        if($save)
        {
            return redirect(route('admin.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $data = Admin::find($id);
        // dd($data);
        return view('admin.update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $check = Admin::find($id);   

        if($check)
        {
            if($request->hasFile('picture'))
            {
                $image = $request->file('picture');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['picture']=$name;
            }
            $data["fname"] = request('fname');
            $data["lname"] = request('lname');
            $data["email"] = request('email');
            $data["mobile"] = request('mobile');
    
            $updated = Admin::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('admin.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }

    public function delete_data($id)
    {
        $check = Admin::find($id); 
        if($check)
        {
             Admin::whereId($id)->delete();
             return redirect(route('admin.index'));   
        }else
        {
            dd("not delete");   
        }

    }
     public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = Admin::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('admin.index'));       
            }   

    }
}
