<?php



namespace App\Http\Controllers;

use App\Customer;

use Illuminate\Http\Request;

use App\Item;



class CustomerController extends Controller

{

    public function __construct()

    {


    }


    public function index()

    {
        // dd("index");
        $customer = Customer::get();
        return view('customer.index', compact('customer'));

    }

    public function create()

    {
        return view('customer.add');
    }

    public function store(Request $request)
    {
        request()->validate([

            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required', 
            'mobile' => 'required', 
            'picture' => 'required' 
        ]);
        // dd($request->all());
        if($request->hasFile('picture'))
        {
            $image = $request->file('picture');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(
            "fname" => request('fname'),
            "lname" => $request->lname,
            "email" => $request->email,
            "mobile" => $request->mobile,
            "picture"=> $image_add,
        );


        $save = Customer::insert($data);
        if($save)
        {
            return redirect(route('customer.index'));
        }
        
    }

    public function show()

    {


    }

    public function edit($id)

    {
        $data = Customer::find($id);
        // dd($data);
        return view('customer.update',compact('data'));
    }

    public function update(Request $request,$id)
    {
        // dd($request->all());
        $check = Customer::find($id);   

        if($check)
        {
            if($request->hasFile('picture'))
            {
                $image = $request->file('picture');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['picture']=$name;
            }
            $data["fname"] = request('fname');
            $data["lname"] = request('lname');
            $data["email"] = request('email');
            $data["mobile"] = request('mobile');
    
            $updated = Customer::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('customer.index'));       
            }   
        }
    }

    public function destroy()
    {


    }

    public function delete_data($id)
    {
        $check = Customer::find($id); 
        if($check)
        {
             Customer::whereId($id)->delete();
             return redirect(route('customer.index'));   
        }else
        {
            dd("not delete");   
        }

    }
     public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');    
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = Customer::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('customer.index'));       
            }   

    }

    public function search(Request $request)
    {
        $search =$request->search;
        $customer = Customer::where('fname', 'LIKE', "%$search%")
        ->orWhere('lname', 'LIKE', "%$search%")
        ->orWhere('mobile', 'LIKE', "%$search%")
        ->orWhere('email', 'LIKE', "%$search%")
        ->get();

        // dd($customer);
           
        
        return view('customer.search', compact('customer'));
    }
    

    

    
}