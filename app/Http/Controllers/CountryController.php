<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = Country::get();
        return view('country.index', compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('country.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('c_flag'))
        {
            $image = $request->file('c_flag');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(

            "c_name" => request('c_name'),
            "c_description" => request('c_description'),
            "c_code" => request('c_code'),
            "c_flag" => $image_add
        );

        $save = Country::insert($data);
        if ($save) {
            
            return redirect(route('country.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Country::find($id);
        return view('country.update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Country::find($id);   

        if($check)
        {
            if($request->hasFile('c_flag'))
            {
                $image = $request->file('c_flag');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['c_flag']=$name;
            }
            $data["c_name"] = request('c_name');
            $data["c_description"] = request('c_description');
            $data["c_code"] = request('c_code');
            // dd($data);
    
            $updated = Country::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('country.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }

     public function delete_data($id)
    {
        $check = Country::find($id); 
        if($check)
        {
             Country::whereId($id)->delete();
             return redirect(route('country.index'));   
        }else
        {
            dd("not delete");   
        }
    }

    public function status(Request $request)

    {
        $id = request('id');

        if (request('status')==1) {
            $data["status"]=0;
        }else{
            $data['status']=1;
        }

        $updated = Country::whereId($id)->update($data);
        if ($updated) {
            return redirect(route('country.index'));
        }
    }
}
