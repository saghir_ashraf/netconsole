<?php

namespace App\Http\Controllers;
use App\Post;
use App\Category;
use App\SubCategory; 
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_cat = SubCategory::get();
        return view('subcategory.index',compact('sub_cat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category= Category::get();
        return view('subcategory.add', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'name' => 'required',
            'description' => 'required', 
            'image' => 'required' 
        ]);
        // dd($request->all());
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(
            "name" => request('name'),
            "description" => request('description'),
            "slug" => str_slug(request('name'),'-'),
            "image"=> $image_add,
            "catId"=>$request->catId,
        );


        $save = SubCategory::insert($data);
        if($save)
        {
            return redirect(route('sub_cat.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data = SubCategory::with('category')->find($id);
        // dd($data);
        $category= Category::get();
        return view('subcategory.update',compact('data','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = SubCategory::find($id);   

        if($check)
        {
            if($request->hasFile('image'))
            {
                $image = $request->file('image');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['image']=$name;
            }
            $data["name"] = request('name');
            $data["description"] = request('description');
            $data['slug'] = str_slug(request('name'),'-');
            $data["catId"] = request('catId');
    
            $updated = SubCategory::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('sub_cat.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(SubCategory $post)
    {
        //
    }

     public function delete_data($id)
    {
        $check = SubCategory::find($id); 
        // dd($check);
        if($check)
        {
             SubCategory::whereId($id)->delete();
             return redirect(route('sub_cat.index'));   
        }else
        {
            dd("not delete");   
        }

    }
     public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = SubCategory::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('post.index'));       
            }   

    }

    public function search(Request $request)
    {
        $search =$request->search;
        $post = SubCategory::with('category')->whereHas('category', function ($query) use ($search){
            $query->where('catName', 'like', '%'.$search.'%');
        })
        ->orWhere('name', 'LIKE', "%$search%")
        ->orWhere('description', 'LIKE', "%$search%")
        ->get();

        // dd($post);
           // 
        
        return view('post.search', compact('post'));
    }
    
}


