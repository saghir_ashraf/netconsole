<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::with('category')->get();
        // dd($post);
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category= Category::get();
        return view('post.add', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        request()->validate([

            'name' => 'required',
            'description' => 'required', 
            'image' => 'required' 
        ]);
        // dd($request->all());
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(
            "name" => request('name'),
            "description" => request('description'),
            "slug" => str_slug(request('name'),'-'),
            "image"=> $image_add,
            "catId"=>$request->catId,
        );


        $save = Post::insert($data);
        if($save)
        {
            return redirect(route('post.index'));
        }
        
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Post::with('category')->find($id);
        // dd($data);
        $category= Category::get();
        return view('post.update',compact('data','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
        public function update(Request $request,$id)
    {
        // dd($request->all());
        $check = Post::find($id);   

        if($check)
        {
            if($request->hasFile('picture'))
            {
                $image = $request->file('picture');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['picture']=$name;
            }
            $data["name"] = request('name');
            $data["description"] = request('description');
            $data['slug'] = str_slug(request('name'),'-');
            $data["catId"] = request('catId');
    
            $updated = Post::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('post.index'));       
            }   
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

     public function delete_data($id)
    {
        $check = Post::find($id); 
        if($check)
        {
             Post::whereId($id)->delete();
             return redirect(route('post.index'));   
        }else
        {
            dd("not delete");   
        }

    }
     public function status(Request $request)
    {
        // dd($request->all());
        $id = request('id');
       
        if (request('status')==1) {
             $data["status"] = 0;
        }else{
            $data["status"] = 1;
        }

        $updated = Post::whereId($id)->update($data);
          if($updated)
            {
                return redirect(route('post.index'));       
            }   

    }

    public function search(Request $request)
    {
        $search =$request->search;
        $post = Post::with('category')->whereHas('category', function ($query) use ($search){
            $query->where('catName', 'like', '%'.$search.'%');
        })
        ->orWhere('name', 'LIKE', "%$search%")
        ->orWhere('description', 'LIKE', "%$search%")
        ->get();

        // dd($post);
           // 
        
        return view('post.search', compact('post'));
    }
    
}
