<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::get();
        return view('employee.index', compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('employee.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('picture'))
        {
            $image = $request->file('picture');
            $name = time().".".$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath,$name);
            $image_add=$name;
        }else
        {
            $image_add="default.png";
        }
        $data = array(

            "fname" => request('fname'),
            "lname" => request('lname'),
            "email" => request('email'),
            "mobile" => request('mobile'),
            "picture" => $image_add
        );

        $save = Employee::insert($data);
        if ($save) {
            
            return redirect(route('employee.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::find($id);
        return view('employee.update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $check = Employee::find($id);   

        if($check)
        {
            if($request->hasFile('picture'))
            {
                $image = $request->file('picture');
                $name = time().".".$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath,$name);
                $data['picture']=$name;
            }
            $data["fname"] = request('fname');
            $data["lname"] = request('lname');
            $data["email"] = request('email');
            $data["mobile"] = request('mobile');
    
            $updated = Employee::whereId($id)->update($data);
            if($updated)
            {
                return redirect(route('employee.index'));       
            }   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }

    public function delete_data($id)
    {
        $check = Employee::find($id); 
        if($check)
        {
             Employee::whereId($id)->delete();
             return redirect(route('employee.index'));   
        }else
        {
            dd("not delete");   
        }
    }

    public function status(Request $request)

    {
        $id = request('id');

        if (request('status')==1) {
            $data["status"]=0;
        }else{
            $data['status']=1;
        }

        $updated = Employee::whereId($id)->update($data);
        if ($updated) {
            return redirect(route('employee.index'));
        }
    }
}
