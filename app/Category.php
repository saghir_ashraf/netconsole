<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public function posts()
    {
    	// return hasMany('App/Post','catId');
    	return $this->hasMany(Post::class,"catId");
    }
}
