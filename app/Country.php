<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public function cities()
    {
    	// return hasMany('App/Post','catId');
    	return $this->hasMany(City::class,"id");
    }
}
